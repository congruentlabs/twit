import argparse
import asyncio
import json
import time
from asyncio import CancelledError, create_task

from datetime import datetime
from json import JSONEncoder
from typing import List, Optional

from aiohttp import web
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class Tweet:

    def __init__(self, tweet_type, text, date):
        self.tweet_type: str = tweet_type
        self.text: str = text
        self.date: datetime = date


tweets: List[Tweet] = []


def get_tweets(twitterHandle: str, timestamp: Optional[datetime]) -> datetime:
    options = Options()
    options.headless = True

    browser = webdriver.Firefox(options=options)

    browser.get(f'https://twitter.com/{twitterHandle.lstrip("@")}')

    newTweets: List[Tweet] = []

    completed = False

    try:
        elements: List[WebElement] = WebDriverWait(browser, 5).until(
            expected_conditions.presence_of_all_elements_located((
                By.XPATH,
                '//div[@data-testid=\'tweet\']'
            ))
        )

        for element in elements:
            try:
                try:
                    element.find_element_by_xpath(
                        'preceding-sibling::div//span[@data-testid=\'socialContext\']'
                        '[text()[contains(., \'Retweeted\')]]'
                    )

                    tweet_type = 're-tweet'
                except NoSuchElementException:
                    tweet_type = 'tweet'

                title = element.find_element_by_xpath('div[2]/div[1]')
                body = element.find_element_by_xpath('div[2]/div[2]/div[1]')

                date = datetime.strptime(
                    title.find_element_by_tag_name('time').get_attribute('datetime'),
                    '%Y-%m-%dT%H:%M:%S.%fZ'
                )

                text = ''.join(map(lambda span: span.text, body.find_elements_by_xpath('descendant::*[text()]')))

                if timestamp is None or date > timestamp:
                    if text != '':
                        newTweets.append(Tweet(tweet_type=tweet_type, text=text, date=date))

            except NoSuchElementException:
                pass

    except TimeoutException:
        browser.quit()
        print('Tweet reader timed out. Either handle has no tweets or unknown error encountered.')
        return datetime.utcnow()

    newest_date: datetime = max(map(lambda tweet: tweet.date, newTweets)) if len(newTweets) > 0 else None

    if timestamp is None and len(newTweets) >= 5:
        completed = True

    if timestamp is not None and newest_date is not None and newest_date <= timestamp:
        completed = True

    if timestamp is not None and len([tweet for tweet in tweets if tweet.date <= timestamp]) > 0:
        completed = True

    oldest_date: datetime = min(map(lambda tweet: tweet.date, newTweets)) if len(newTweets) > 0 else None

    while not completed:
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(5)

        new_elements = browser.find_elements_by_xpath('//div[@data-testid=\'tweet\']')

        for new_element in new_elements:
            try:
                try:
                    new_element.find_element_by_xpath(
                        'preceding-sibling::div//span[@data-testid=\'socialContext\']'
                        '[text()[contains(., \'Retweeted\')]]'
                    )

                    tweet_type = 're-tweet'
                except NoSuchElementException:
                    tweet_type = 'tweet'

                title = new_element.find_element_by_xpath('div[2]/div[1]')
                date = datetime.strptime(
                    title.find_element_by_tag_name('time').get_attribute('datetime'),
                    '%Y-%m-%dT%H:%M:%S.%fZ'
                )

                if oldest_date is not None and date <= oldest_date:
                    continue

                body = new_element.find_element_by_xpath('div[2]/div[2]/div[1]')
                text = ''.join(map(lambda span: span.text, body.find_elements_by_xpath('descendant::*[text()]')))

                if text != '':
                    newTweets.append(Tweet(tweet_type=tweet_type, text=text, date=date))

            except NoSuchElementException:
                pass

        if len(newTweets) > 0:
            if oldest_date == min(map(lambda tweet: tweet.date, newTweets)):
                completed = True

            else:
                oldest_date = min(map(lambda tweet: tweet.date, newTweets))

            if timestamp is None and len(newTweets) >= 5:
                completed = True

            if timestamp is not None and len([tweet for tweet in newTweets if tweet.date < timestamp]) > 0:
                completed = True

        else:
            completed = True

        newest_date: datetime = max(map(lambda tweet: tweet.date, newTweets)) if len(newTweets) > 0 else None

    browser.quit()

    newTweets.sort(key=lambda tweet: tweet.date, reverse=True)

    if timestamp is None:
        tweetsToPrint = newTweets[:5]

    else:
        tweetsToPrint = list(filter(lambda tweet: tweet.date > timestamp, newTweets))

    tweetsToPrint.reverse()
    tweets.extend(tweetsToPrint)

    if len(tweetsToPrint) > 0:
        for tweet in tweetsToPrint:
            print(f'{twitterHandle} {"re-tweeted" if tweet.tweet_type == "re-tweet" else "tweeted"} '
                  f'"{tweet.text}" at {datetime.strftime(tweet.date, "%Y-%m-%dT%H:%M:%S.%fZ")}')

    else:
        print(f'{twitterHandle} has not tweeted any new tweets since '
              f'{datetime.strftime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")}')

    return newest_date if newest_date is not None else timestamp if timestamp is not None else datetime.utcnow()


if __name__ == '__main__':

    description = 'This is a program for monitoring tweets.'

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('handle', help='The twitter handle to monitor.')

    args = parser.parse_args()


    class DateTimeEncoder(JSONEncoder):

        def default(self, obj):
            if isinstance(obj, datetime):
                return datetime.strftime(obj, "%Y-%m-%dT%H:%M:%S.%fZ")


    async def get_tweets_handler(_: web.Request) -> web.Response:
        return web.json_response(
            json.dumps(
                list(map(lambda tweet: tweet.__dict__, tweets)),
                cls=DateTimeEncoder
            )
        )


    async def schedule_tweet_reads():
        try:
            oldest: datetime = get_tweets(args.handle, None)

            while True:
                await asyncio.sleep(600)

                oldest = get_tweets(args.handle, oldest)
        except CancelledError:
            pass


    async def start_background_tasks(application):
        application['tweet_scheduler'] = create_task(schedule_tweet_reads())


    async def cleanup_background_tasks(application):
        application['tweet_scheduler'].cancel()
        await application['tweet_scheduler']


    app = web.Application()
    app.router.add_get('/tweets.json', get_tweets_handler)
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    web.run_app(app, host='127.0.0.1', port=8080)
