# Twit

## running

### CLI
usage: Twit.py [-h] handle

### API

The program exposes a http server at http://127.0.0.1:8080

Calls to http://127.0.0.1:8080/tweets.json will return a json object of all the tweets currently held in memory.

The application requires Firefox and geckodriver to be installed.

## assumptions

Looking at the XHR of the website, tweets were retrieved via the API using OAuth as a guest user when no account was logged in.

I took this to still classify as user authentication so took a scraping approach. As it was a dynamic website using the react framework, I decided to use selenium as this allowed for the rendering and navigation of the website.